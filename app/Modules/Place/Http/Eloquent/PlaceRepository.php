<?php
namespace App\Modules\Place\Http\Eloquent;

use App\Models\Place;
use App\Modules\Place\Http\Interfaces\PlaceRepositoryInterface;

class PlaceRepository implements PlaceRepositoryInterface
{
    public function addPlace($request)
    {
            $place = Place::create([
                'place_category_id' => $request->place_category_id,
                'name' => $request->name,
                'present_type' => $request->present_type,
                'price' => $request->price,
                'provider_id' => $request->provider_id,
                'quantity' => $request->quantity,
                'date_from' => $request->date_from,
                'date_to' => $request->date_to,
                'lat' => $request->lat,
                'lng' => $request->lng,
                'address' => $request->address,
                'desc_en' => $request->desc_en,
                'desc_ar' => $request->desc_ar,
            ]);
        for ($i = 0; $i < count($request->imgs); $i++) {
            $place->related_images()->create(['image' => $request->imgs[$i],
                'default_image' => 0,

            ]);
        }

    }

}
