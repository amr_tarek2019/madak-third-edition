<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('/place', function (Request $request) {
//    // return $request->place();
//})->middleware('auth:api');
Route::group(['prefix' => 'place'], function () {
    Route::post('add','Api\PlaceController@addPlace');
});
