<?php
/**
 * Created by PhpStorm.
 * User: grand
 * Date: 8/25/2019
 * Time: 9:16 AM
 */
namespace App\Modules\Auth\Http\Interfaces;

interface AuthRepositoryInterface
{
    public function SendVerificationCode($request);

    public function VerifyCode($request);

    public function CheckJwt($jwt);
//    public function GetCountryNameById($country_id=null,$lang = "en");
//    public function GetCityNameById($city_id=null,$lang = "en");
    public function ChangePassword($request);

}
