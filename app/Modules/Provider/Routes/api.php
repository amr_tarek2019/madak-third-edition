<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('/provider', function (Request $request) {
//    // return $request->provider();
//})->middleware('auth:api');

Route::group(['prefix' => 'provider'], function () {
  Route::post('request','Api\ProviderController@serviceProviderRequest');
    Route::get('/getPlacesandservices','Api\ProviderController@getProviderPlacesAndServices');
    Route::get('/allplaces/{place_category_id}','Api\ProviderController@GetAllPlacesCategories');
});
