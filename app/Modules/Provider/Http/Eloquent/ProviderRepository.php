<?php
namespace App\Modules\Provider\Http\Eloquent;

use App\Models\Place;
use App\Models\Providers;
use App\Models\ServiceProviderRequest;
use App\Models\User;
use App\Modules\Provider\Http\Interfaces\ServiceProviderRepositoryInterface;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use function foo\func;

class ProviderRepository implements ServiceProviderRepositoryInterface
{
    public function CheckJwt($jwt)
    {
        $user=User::where('jwt_token',$jwt)->first();
        if (isset($user->id)){
            return $user;
        }else{
            return "false";
        }
    }

    public function serviceProviderRequest($request)
{
    $validator = Validator::make($request->all(),
        array(
            'Jwt' => 'required'
        )
    );

    if ($validator->fails()) {
        return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
    }
    $user = $this->CheckJwt($request->Jwt);
    $request['user_id'] = $user->id;

    ServiceProviderRequest::create($request->all());
    return 'true';
}

    public function getProviderPlacesAndServices($user_id){
        $Jwt = getallheaders()['Jwt'];
        $user=User::where('jwt_token',$Jwt)->select('id')->first();
        $provider=Providers::where('user_id',$user->id)->select('id')->first();
            $places= $provider->related_places()->select('id', 'name')->withCount('place_category_id')->get();
            return $places;
        $services= $provider->related_services()->select('id', 'present_type')->with(['related_images'])->get();
            return $services;

    }

    public function GetAllPlacesCategories($lang,$place_category_id){
        $placeCategory = Place::whereId($place_category_id)->first();
        $places= $placeCategory->related_category()->
        select('id')->
            orderBy('id','desc')->with(['related_images'])
            ->get();
        return $places;

    }
}
