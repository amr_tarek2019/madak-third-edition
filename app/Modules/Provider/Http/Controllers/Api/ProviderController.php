<?php

namespace App\Modules\Provider\Http\Controllers\Api;

use App\Models\Providers;
use App\Models\User;
use Illuminate\Http\Request;
use App\Modules\Provider\Http\Interfaces\ServiceProviderRepositoryInterface;
use App\Modules\Auth\Http\Interfaces\AuthRepositoryInterface;
use App\Http\Controllers\Controller;

class ProviderController extends Controller
{
    protected $providerObject;
    protected $userObject;

    public function __construct(ServiceProviderRepositoryInterface $providerRepository,AuthRepositoryInterface $userRepository)
    {
        $this->providerObject = $providerRepository;
        $this->userObject = $userRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function serviceProviderRequest(Request $request)
    {
        $lang = getallheaders()['Lang'];
        $Jwt = getallheaders()['Jwt'];
        $request['Jwt']=$Jwt;
        $result=$this->userObject->CheckJwt($Jwt);
        if($result=='false'){
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $this->providerObject->serviceProviderRequest($request);
        return response()->json(res_msg($lang, success(), 200, 'request_sent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getProviderPlacesAndServices()
    {
        $lang = getallheaders()['Lang'];

        $Jwt = getallheaders()['Jwt'];
        $result = $this->userObject->CheckJwt($Jwt);
        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $provider=Providers::where('user_id',$result->id)->pluck('id')->first();
        $results['places'] = $this->providerObject->getProviderPlacesAndServices($provider);
        $results['preparations'] = $this->providerObject->getProviderPlacesAndServices($provider);
        return response()->json(res_msg($lang, success(), 200, 'all_places', $results));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function GetAllPlacesCategories($place_category_id)
    {
        $lang = getallheaders()['Lang'];
        $result = $this->providerObject->GetAllPlacesCategories($lang, $place_category_id);
        return response(res_msg($lang, success(), 200, 'all_places', $result));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
