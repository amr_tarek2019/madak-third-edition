<?php
namespace App\Modules\Provider\Http\Interfaces;

interface ServiceProviderRepositoryInterface
{
    public function serviceProviderRequest($request);
    public function getProviderPlacesAndServices($user_id);
    public function GetAllPlacesCategories($lang,$place_category_id);
}
