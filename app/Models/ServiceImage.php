<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceImage extends Model
{
    use SoftDeletes,HelperTrait ;
    protected $table='services_images';
    protected $fillable=['service_id', 'image'];

    public function service()
    {
        return $this->belongsTo('App\Service','service_id');
    }
    public function getImageAttribute(){
        if($this->attributes['image']!=null) {
            return SiteImages_path('services') . '/original/' . $this->attributes['image'];
        }
        else{

            return SiteImages_path('services') . '/default.png';

        }


    }
    public function setImageAttribute($file)
    {
        if ($file) {
            $fileName = $this->createFileName($file);
            $this->originalImage($file, $fileName,'services/original');
            $this->mediumImage($file, $fileName,150,150,'services/meduim');
            $this->thumbImage($file, $fileName, 70,70,'services/thumbnail');
            $this->attributes['image'] = $fileName;
        }

    }
}
