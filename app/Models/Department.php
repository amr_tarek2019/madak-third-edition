<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table='departments';
    protected $fillable=['logo', 'name_en', 'name_ar'];

    public function getDepartment()
    {
        return static::all();
    }

    public function getLogoAttribute($value)
    {
        if ($value) {
            return asset('uploads/departments/'.$value);
        } else {
            return asset('uploads/user/profile/default.png');
        }
    }

    public function setLogoAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/departments/'),$imageName);
            $this->attributes['logo']=$imageName;
        }
    }
}
