<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Providers extends Model
{
    protected $table='providers';
    protected $fillable=['user_id'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function related_places(){
        return $this->hasMany('App\Models\Place', 'provider_id')
            ->with('related_images');
    }
    public function related_services(){
        return $this->hasMany('App\Models\Service', 'provider_id')
            ->with('related_images');
    }

}
