<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $table='ratings';
    protected $fillable=[ 'user_id', 'place_id', 'rate', 'review'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function place()
    {
        return $this->belongsTo('App\Place','place_id');
    }
}
