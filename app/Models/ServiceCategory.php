<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceCategory extends Model
{
    protected $table='services_categories';
    protected $fillable=['department_id', 'name_en', 'name_ar', 'image'];

    public function department()
    {
        return $this->belongsTo('App\Department','department_id');
    }
}
