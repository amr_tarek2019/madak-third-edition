<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $table='places';
    protected $fillable=['provider_id', 'place_category_id',
        'name', 'present_type', 'price', 'quantity', 'date_from',
        'date_to', 'lat', 'lng', 'address', 'desc_en', 'desc_ar'];

    public function provider()
    {
        return $this->belongsTo('App\Provider','provider_id');
    }

    public function placeCategory()
    {
        return $this->belongsTo('App\PlaceCategory','place_category_id');
    }

    public function related_images(){

        return $this->hasMany(PlaceImage::class, 'place_id', 'id')->select('image','place_id');
    }

    public function related_provider(){
        return $this->belongsTo(Providers::class, 'provider_id','provider_id');
    }

    public function related_category(){
        return $this->hasMany('App\Models\PlaceCategory', 'place_category_id','id');
    }

    public function GetImage($place){
        return $place->related_default_image;
    }


}
