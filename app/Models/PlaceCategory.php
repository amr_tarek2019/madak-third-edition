<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlaceCategory extends Model
{
    protected $table='places_categories';
    protected $fillable=['department_id', 'image', 'name_en', 'name_ar'];

    public function department()
    {
        return $this->belongsTo('App\Department','department_id');
    }
}
