<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceSubcategory extends Model
{
    protected $table='services_subcategories';
    protected $fillable=['service_category_id', 'name_en', 'name_ar', 'image'];

    public function serviceCategory()
    {
        return $this->belongsTo('App\ServiceCategory','service_category_id');
    }
}
