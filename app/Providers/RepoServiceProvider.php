<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            'App\Modules\Auth\Http\Interfaces\AuthRepositoryInterface',
            'App\Modules\Auth\Http\Eloquent\AuthRepository'
        );
        $this->app->bind(
            'App\Modules\Provider\Http\Interfaces\ServiceProviderRepositoryInterface',
            'App\Modules\Provider\Http\Eloquent\ProviderRepository'
        );
        $this->app->bind(
            'App\Modules\Place\Http\Interfaces\PlaceRepositoryInterface',
            'App\Modules\Place\Http\Eloquent\PlaceRepository'
        );
        $this->app->bind(
            'App\Modules\Service\Http\Interfaces\ServiceRepositoryInterface',
            'App\Modules\Service\Http\Eloquent\ServiceRepository'
        );


    }
}
